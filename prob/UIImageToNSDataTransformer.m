//
//  UIImageToNSDataTransformer.m
//  prob
//
//  Created by stronger on 11/9/23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "UIImageToNSDataTransformer.h"

@implementation UIImageToNSDataTransformer

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(Class)transformedValueClass
{
    return [NSData class];
}

+(BOOL)allowsReverseTransformation
{
    return YES;
}

-(id)transformedValue:(id)value
{
    return UIImagePNGRepresentation(value);
}

-(id)reverseTransformedValue:(id)value
{
    return [[[UIImage alloc] initWithData: value] autorelease];
}

@end
