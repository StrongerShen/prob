//
//  RootViewController.h
//  prob
//
//  Created by stronger on 11/9/18.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AddCustomerController.h"
#import "DisplayCustomerController.h"
#import "MasterProductInfoController.h"

@interface RootViewController : UITableViewController <NSFetchedResultsControllerDelegate, AddCustomerControllerDelegate, DisplayCustomerControllerDelegate> {

    NSFetchedResultsController *fetchedResultsController;
    NSManagedObjectContext *managedObjectContext;

    IBOutlet UISearchBar *srchbar;
    IBOutlet UIToolbar *toolbar;
}

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) IBOutlet UISearchBar *srchbar;
@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;

-(IBAction)addCustomer;
-(IBAction)showMasterInfo;

@end
