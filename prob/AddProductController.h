//
//  AddProductController.h
//  prob
//
//  Created by stronger on 11/9/22.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@protocol AddProductControllerDelegate;

@interface AddProductController : UIViewController {
    id <AddProductControllerDelegate> delegate;
    IBOutlet UIBarButtonItem *cancelButton;
    IBOutlet UIBarButtonItem *saveButton;
    IBOutlet UITextField *newitemname;
    IBOutlet UITextField *newquantity;
    IBOutlet UITextField *newprice;
    Product *prod;
}

@property(nonatomic, retain) Product *prod;
@property(nonatomic, retain) id <AddProductControllerDelegate> delegate;
@property(nonatomic, retain) IBOutlet UIBarButtonItem *cancelButton;
@property(nonatomic, retain) IBOutlet UIBarButtonItem *saveButton;
@property(nonatomic, retain) IBOutlet UITextField *newitemname;
@property(nonatomic, retain) IBOutlet UITextField *newquantity;
@property(nonatomic, retain) IBOutlet UITextField *newprice;

-(IBAction)cancel:(id)sender;
-(IBAction)save:(id)sender;

@end

@protocol AddProductControllerDelegate

-(void)addproductController:(AddProductController *)controller selectedsave:(BOOL)save;

@end