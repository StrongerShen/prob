//
//  SearchProductController.h
//  prob
//
//  Created by stronger on 11/10/10.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "probAppDelegate.h"
#import "MasterProduct.h"
#import "AddProductController.h"
#import "Customer.h"

@interface SearchProductController : UIViewController <NSFetchedResultsControllerDelegate> {
    NSFetchedResultsController *fetchedResultsController;
    Customer *cust;
    IBOutlet UISearchBar *srchBar;
    UITableView *tableView;
    NSMutableArray *mastproduts;
    IBOutlet UIBarButtonItem *cancelButton;
}

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) Customer *cust;
@property (nonatomic, retain) IBOutlet UISearchBar *srchBar;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *mastproduts;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *cancelButton;

-(IBAction)cancel:(id)sender;

@end
