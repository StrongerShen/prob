//
//  Product.h
//  prob
//
//  Created by stronger on 11/9/22.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

//james

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface Product : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * itemname;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) Customer *customer;

@end
