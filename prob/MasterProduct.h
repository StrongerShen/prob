//
//  MasterProduct.h
//  prob
//
//  Created by stronger on 11/9/23.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MasterProduct : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSString * itemname;
@property (nonatomic, retain) id image;

@end
