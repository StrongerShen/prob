//
//  ProductInfoController.h
//  prob
//
//  Created by stronger on 11/9/22.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customer.h"
#import "Product.h"
#import "AddProductController.h"

@interface ProductInfoController : UIViewController <NSFetchedResultsControllerDelegate, AddProductControllerDelegate> {
    Customer *cust;
    Product *prod;
    UITableView *tableView;
    NSMutableArray *products;
    NSFetchedResultsController *fetchedResultsController;
    IBOutlet UIBarButtonItem *backButton;
    IBOutlet UIBarButtonItem *editButton;
    IBOutlet UIBarButtonItem *addButton;
}

@property(nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@property(nonatomic, retain) Customer *cust;
@property(nonatomic, retain) Product *prod;
@property(nonatomic, retain) NSMutableArray *products;

@property(nonatomic, retain) UIBarButtonItem *backButton;
@property(nonatomic, retain) UIBarButtonItem *editButton;
@property(nonatomic, retain) UIBarButtonItem *addButton;
@property(nonatomic, retain) IBOutlet UITableView *tableView;

-(IBAction)back:(id)sender;
-(IBAction)addproduct:(id)sender;
-(IBAction)editproduct:(id)sender;

@end
