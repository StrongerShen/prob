//
//  ProductInfoController.m
//  prob
//
//  Created by stronger on 11/9/22.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ProductInfoController.h"

@implementation ProductInfoController

@synthesize backButton, addButton, editButton, tableView;
@synthesize cust, prod, products;
@synthesize fetchedResultsController;

-(IBAction)back:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)addproduct:(id)sender
{
    AddProductController *addprodController = [[AddProductController alloc] initWithNibName:@"AddProductController" bundle:nil];
    addprodController.delegate = self;
    UINavigationController *navigController = [[UINavigationController alloc] initWithRootViewController:addprodController];
    [self presentModalViewController:navigController animated:YES];
    
    NSManagedObjectContext *contex = [cust managedObjectContext];
    prod = [NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:contex];
    [cust addProductObject:prod];
    addprodController.prod = prod;
    [addprodController release];
    [navigController release];
}

-(IBAction)editproduct:(id)sender
{
    if ([editButton.title isEqualToString:@"Edit"]) {
        editButton.title = @"Done";
        [self.tableView setEditing:YES animated:YES];
    } else {
        editButton.title =@"Edit";
        [self.tableView setEditing:NO animated:YES];
    }
}

-(void)addproductController:(AddProductController *)controller selectedsave:(BOOL)save
{
    NSManagedObjectContext *contex = [cust managedObjectContext];
    if (!save) {
        [cust removeProductObject:prod];
    }
    
    NSError *error;
    if (![contex save:&error]) {
        NSLog(@"Unresolved error %@ %@", error, [error userInfo]);
        exit(-1);
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - 書本遺漏的部分
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	NSMutableArray *sortedproducts=[[NSMutableArray alloc] initWithArray:[cust.product allObjects]];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemname" ascending:YES];
	NSArray *sortDescriptors =[[NSArray alloc] initWithObjects:sortDescriptor,nil];	
	[sortedproducts sortUsingDescriptors:sortDescriptors];
    
	self.products=sortedproducts;
    
	self.tableView.rowHeight=44.0;
	[sortDescriptor release];
	[sortDescriptors release];
	[self.tableView reloadData];	 
}
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = cust.name;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(void)dealloc
{
    [fetchedResultsController release];
    
    [cust release];
    [prod release];
    [products release];
    
    [backButton release];
    [editButton release];
    [addButton release];
    [tableView release];
    
    [super dealloc];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cust.product count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
    Product *prod = [products objectAtIndex:indexPath.row];
    cell.textLabel.text = prod.itemname;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
         NSManagedObjectContext *contxt = [fetchedResultsController managedObjectContext];
         [contxt deleteObject:[fetchedResultsController objectAtIndexPath:indexPath]];
         Product *prod = [products objectAtIndex:indexPath.row];
         [cust removeProductObject:prod];
         [products removeObject:prod];
         NSManagedObjectContext *contex = prod.managedObjectContext;
         [contex deleteObject:prod];
         
         [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
     }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}

-(NSFetchedResultsController *)fetchedResultsController
{
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *contex = [cust managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Customer" inManagedObjectContext:contex];
    [fetchRequest setEntity:entity];

    // Edit the section name key path and cache name if appropriate. 
    // nil for section name key path means “no sections”.
    NSFetchedResultsController *aFetchedResultsController = 
        [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                            managedObjectContext:contex 
                                              sectionNameKeyPath:nil
                                                       cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;

    [aFetchedResultsController release];
    [fetchRequest release];
    
    return fetchedResultsController;
}



@end
