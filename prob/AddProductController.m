//
//  AddProductController.m
//  prob
//
//  Created by stronger on 11/9/22.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "AddProductController.h"

@implementation AddProductController

@synthesize newitemname, newquantity, newprice;
@synthesize prod, delegate, saveButton, cancelButton;


-(IBAction)cancel:(id)sender
{
    [delegate addproductController:self selectedsave:NO];
}

-(IBAction)save:(id)sender
{
    prod.itemname = newitemname.text;
    prod.quantity = [NSNumber numberWithInt:(int) [newquantity.text intValue]];
    prod.price = [NSNumber numberWithInt:(int) [newprice.text intValue]];
    [delegate addproductController:self selectedsave:YES];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"New Product";
    self.navigationItem.rightBarButtonItem = saveButton;
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc
{
    [newitemname release];
    [newquantity release];
    [newprice release];
    [saveButton release];
    [cancelButton release];
    [prod release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
