//
//  EditMasterProductController.m
//  prob
//
//  Created by stronger on 11/9/28.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "EditMasterProductController.h"

@implementation EditMasterProductController

@synthesize mastprod;
@synthesize itemname, quantity, price, prodimage, backbutton;

#pragma mark - added method

-(void)back:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)changeimage:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:YES];
    [imagePicker release];
}

#pragma mark - UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    prodimage.image = image;
    mastprod.image =image;
    [self dismissModalViewControllerAnimated:YES];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - app lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc
{
    [mastprod release];
    [itemname release];
    [quantity release];
    [price release];
    [prodimage release];
    [backbutton release];

    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Master Product";

    itemname.text = mastprod.itemname;

    NSString *str = [NSString stringWithFormat:@"%d", [mastprod.quantity intValue]];
    quantity.text = str;

    str = [NSString stringWithFormat:@"%f", [mastprod.price floatValue]];
    price.text = str;

    prodimage.image = mastprod.image;
    
    itemname.enabled = NO;
    quantity.enabled = NO;
    price.enabled = NO;
    
    self.navigationItem.leftBarButtonItem =backbutton;
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    itemname.enabled = YES;
    quantity.enabled = YES;
    price.enabled = YES;
    
    if (!editing) {
        mastprod.itemname = itemname.text;
        mastprod.quantity = [NSNumber numberWithInt:[quantity.text intValue]];
        mastprod.price = [NSNumber numberWithFloat:[price.text floatValue]];
        itemname.enabled = NO;
        quantity.enabled = NO;
        price.enabled = NO;
        
        NSError *error;
        NSManagedObjectContext *contex = [mastprod managedObjectContext];
        if (![contex save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
