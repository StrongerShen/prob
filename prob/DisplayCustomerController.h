//
//  DisplayCustomerController.h
//  prob
//
//  Created by stronger on 11/9/19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customer.h"
#import "ProductInfoController.h"

@protocol DisplayCustomerControllerDelegate;

@interface DisplayCustomerController : UIViewController {
    Customer *cust;
    id <DisplayCustomerControllerDelegate> delegate;
    IBOutlet UITextField *name;
    IBOutlet UITextField *emailid;
    IBOutlet UITextField *contactno;
}

@property (nonatomic, retain) Customer *cust;
@property (nonatomic, retain) id <DisplayCustomerControllerDelegate> delegate;
@property (nonatomic, retain) IBOutlet UITextField *name;
@property (nonatomic, retain) IBOutlet UITextField *emailid;
@property (nonatomic, retain) IBOutlet UITextField *contactno;
-(IBAction)showproduct:(id)sender;

@end


@protocol DisplayCustomerControllerDelegate

-(void)displaycust:(DisplayCustomerController *)controller selecteddone:(BOOL) done;

@end