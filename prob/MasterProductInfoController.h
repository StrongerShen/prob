//
//  MasterProductInfoController.h
//  prob
//
//  Created by stronger on 11/9/24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterProduct.h"
#import "AddMasterProductController.h"
#import "probAppDelegate.h"
#import "EditMasterProductController.h"

@interface MasterProductInfoController : UIViewController <NSFetchedResultsControllerDelegate, AddMasterProductControllerDelegate> {
    MasterProduct *mastprod;
    NSMutableArray *mastproducts;
    NSFetchedResultsController *fetchedResultsController;
    IBOutlet UIBarButtonItem *custlistButton;
    IBOutlet UIBarButtonItem *editmastButton;
    IBOutlet UIBarButtonItem *addmastButton;
    UITableView *tableView;
}

@property (nonatomic, retain) MasterProduct *mastprod;
@property (nonatomic, retain) NSMutableArray *mastproducts;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *custlistButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *editmastButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *addmastButton;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

-(IBAction)custlist:(id)sender;
-(IBAction)addmastproduct:(id)sender;
-(IBAction)editmastproduct:(id)sender;

@end
