//
//  AddMasterProductController.h
//  prob
//
//  Created by stronger on 11/9/24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterProduct.h"

@protocol AddMasterProductControllerDelegate;

@interface AddMasterProductController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate> {
    id <AddMasterProductControllerDelegate> delegate;
    MasterProduct *mastprod;
    IBOutlet UIBarButtonItem *cancelButton;
    IBOutlet UIBarButtonItem *saveButton;
    IBOutlet UITextField *itemname;
    IBOutlet UITextField *quantity;
    IBOutlet UITextField *price;
    IBOutlet UIImageView *prodimage;
}

@property (nonatomic, retain) id <AddMasterProductControllerDelegate> delegate;
@property (nonatomic, retain) MasterProduct *mastprod;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *saveButton;
@property (nonatomic, retain) IBOutlet UITextField *itemname;
@property (nonatomic, retain) IBOutlet UITextField *quantity;
@property (nonatomic, retain) IBOutlet UITextField *price;
@property (nonatomic, retain) IBOutlet UIImageView *prodimage;

-(IBAction)cancel:(id)sender;
-(IBAction)save:(id)sender;
-(IBAction)selectimagebutton:(id)sender;

@end

@protocol AddMasterProductControllerDelegate
-(void)addmastprodController:(AddMasterProductController*)controller selectedsave:(BOOL)save;
@end

