//
//  SearchProductController.m
//  prob
//
//  Created by stronger on 11/10/10.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "SearchProductController.h"

@implementation SearchProductController

@synthesize fetchedResultsController, cust, srchBar, tableView, mastproduts, cancelButton;

#pragma mark - add methods

-(IBAction)cancel:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - searchbar

-(void)searchBar: (UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ( [[searchBar text] length] > 0 ) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemname CONTAINS %@", [searchBar text] ];
        [fetchedResultsController.fetchRequest setPredicate:predicate];
    } else {
        [fetchedResultsController.fetchRequest setPredicate:nil];
    }
    
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error] ) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo] );
        exit(-1);
    }
    
    [self.tableView reloadData];
    [searchBar resignFirstResponder];
    return;
}

#pragma mark - tableview

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    if (count==0) {
        count = 1;
    }
    return count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger nOfRows = 0;
    
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        nOfRows = [sectionInfo numberOfObjects];
    }
    return nOfRows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
	}

    // Configure the cell.
    
	NSManagedObject *managedObject = [fetchedResultsController objectAtIndexPath:indexPath];
	cell.textLabel.text = [[managedObject valueForKey:@"itemname"] description];
	UIImage *pimage=[managedObject valueForKey:@"image"];
	CGSize size=pimage.size;
	CGFloat ratio = 0;
	if (size.width > size.height) {
		ratio = 44.0 / size.width;
	}
	else {
		ratio = 44.0 / size.height;
	}
	CGRect rect = CGRectMake(0.0, 0.0, ratio * size.width, ratio * size.height);
	UIGraphicsBeginImageContext(rect.size);
	[pimage drawInRect:rect];
	cell.imageView.image=UIGraphicsGetImageFromCurrentImageContext();	
	UIGraphicsEndImageContext();	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	AddProductController *addprodController =[[AddProductController alloc] initWithNibName:@"AddProductController" bundle:nil];
	MasterProduct *selectedprod=(MasterProduct *) [fetchedResultsController objectAtIndexPath:indexPath];
	addprodController.mastprod=selectedprod;
	addprodController.cust=cust;
	[self.navigationController pushViewController:addprodController animated:YES];
	[addprodController release];
}


- (NSFetchedResultsController *)fetchedResultsController {
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
	probAppDelegate *appDelegate = (probAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:@"MasterProduct" inManagedObjectContext:context]];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemname" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
	self.fetchedResultsController = aFetchedResultsController;
	[aFetchedResultsController release];
	return fetchedResultsController;
}    



#pragma mark - App lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc
{
    [fetchedResultsController release];
    [cust release];
    [srchBar release];
    [tableView release];
    [mastproduts release];
    [cancelButton release];
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
