//
//  AddMasterProductController.m
//  prob
//
//  Created by stronger on 11/9/24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "AddMasterProductController.h"

@implementation AddMasterProductController

@synthesize delegate, mastprod, cancelButton, saveButton, itemname, quantity, price, prodimage;


#pragma mark -

-(IBAction)cancel:(id)sender
{
    [delegate addmastprodController:self selectedsave:NO];
}

-(IBAction)save:(id)sender
{
    mastprod.itemname = itemname.text;
    mastprod.quantity = [NSNumber numberWithInt: [quantity.text intValue]];
    mastprod.price = [NSNumber numberWithFloat: [price.text floatValue]];
    [delegate addmastprodController:self selectedsave:YES];
}

-(IBAction)selectimagebutton:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    [self presentModalViewController:imagePicker animated:YES];
    [imagePicker release];
}

#pragma mark - imagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)selectedImage editingInfo:(NSDictionary *)editingInfo
{
    prodimage.image = selectedImage;
    mastprod.image = selectedImage;
    [self dismissModalViewControllerAnimated:YES];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissModalViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==quantity) {
        [quantity resignFirstResponder];
    }
    return YES;
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Add Product";
    self.navigationItem.rightBarButtonItem = saveButton;
    self.navigationItem.leftBarButtonItem = cancelButton;
    quantity.delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc
{
    [mastprod release];
    [cancelButton release];
    [saveButton release];
    [itemname release];
    [quantity release];
    [price release]; 
    [prodimage release];
    [super dealloc];
}


@end
