//
//  DisplayCustomerController.m
//  prob
//
//  Created by stronger on 11/9/19.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "DisplayCustomerController.h"

@implementation DisplayCustomerController

@synthesize cust, name, emailid, contactno;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Customer Information";
    name.text = cust.name;
    emailid.text = cust.emailid;
    contactno.text = cust.contactno;
    
    name.enabled = NO;
    emailid.enabled = NO;
    contactno.enabled = NO;
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [cust addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
}

- (void)viewDidUnload
{
    [cust removeObserver:self forKeyPath:@"name"];
    
    self.cust = nil;
    self.name = nil;
    self.emailid = nil;
    self.contactno = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc
{
    [cust release];
    [name release];
    [emailid release];
    [contactno release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -

-(IBAction)showproduct:(id)sender
{
    ProductInfoController *prodinfo = [[ProductInfoController alloc] initWithNibName:@"ProductInfoController" bundle:nil];
    prodinfo.cust = cust;
    [self presentModalViewController:prodinfo animated:YES];
    [prodinfo release];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    name.enabled = YES;
    emailid.enabled = YES;
    contactno.enabled = YES;
    
    if (!editing) {
        cust.name = name.text;
        cust.emailid = emailid.text;
        cust.contactno = contactno.text;
        name.enabled = NO;
        emailid.enabled = NO;
        contactno.enabled = NO;
        [delegate displaycust:self selecteddone:YES];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"Attribute is %@, Old is %@, New is %@", keyPath, [change valueForKey:NSKeyValueChangeOldKey], [change valueForKey:NSKeyValueChangeNewKey] );
}



@end
