//
//  MasterProductInfoController.m
//  prob
//
//  Created by stronger on 11/9/24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "MasterProductInfoController.h"

@implementation MasterProductInfoController

@synthesize mastprod, mastproducts, fetchedResultsController;
@synthesize custlistButton, editmastButton, addmastButton, tableView;

#pragma mark -

-(IBAction)custlist:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)addmastproduct:(id)sender
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    AddMasterProductController *addmastController = [[AddMasterProductController alloc] initWithNibName:@"AddMasterProductController" bundle:nil];
    addmastController.delegate = self;
    addmastController.mastprod = [NSEntityDescription insertNewObjectForEntityForName:@"MasterProduct" inManagedObjectContext:context];
    UINavigationController *navigatController = [[UINavigationController alloc] initWithRootViewController:addmastController];
    [self presentModalViewController:navigatController animated:YES];
    [addmastController release];
    [navigatController release];
}

-(IBAction)editmastproduct:(id)sender
{
    if ([editmastButton.title isEqualToString:@"Edit"]) {
        editmastButton.title = @"Done";
        [self.tableView setEditing:YES animated:YES];
    } else {
        editmastButton.title = @"Edit";
        [self.tableView setEditing:NO animated:YES];
    }
}

-(void)addmastprodController:(AddMasterProductController *)controller selectedsave:(BOOL)save
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    
    if (!save) {
        [context deleteObject: controller.mastprod];
    }
    
    // Save the context.
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - App lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc
{
    [custlistButton release];
    [editmastButton release];
    [addmastButton release];
    [tableView release];
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Products List";
    
    //Fetch fetchedResultsController
    NSError* error;
    if (![[self fetchedResultsController] performFetch:&error]) {
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"MasterProduct" inManagedObjectContext:context]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemname" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    self.tableView.rowHeight = 44.0;
    
    NSError *error = nil;
    mastproducts = (NSMutableArray *)[context executeFetchRequest:fetchRequest error:&error];

    [sortDescriptors release];
    [sortDescriptor release];
    [fetchRequest release];
    
    [self.tableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [[self.fetchedResultsController sections] count];
    if (count==0) {
        count = 1;
    }
    return count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger nOfRows = 0;
    if ([[self.fetchedResultsController sections] count]>0 ) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section ];
        nOfRows = [sectionInfo numberOfObjects];
    }
    return nOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";

    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    // Configure the cell.
    NSManagedObject *managedObject = [fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[managedObject valueForKey:@"itemname"] description];

    //取得 image 繪到 cell.imageView.view 上面
    UIImage *pimage = [managedObject valueForKey:@"image"];
    CGSize size = pimage.size;
    CGFloat ratio = 0;
    if (size.width > size.height) {
        ratio = 44.0 / size.width;
    } else {
        ratio = 44.0 / size.height;
    }
    CGRect rect = CGRectMake(0.0, 0.0, ratio * size.width, ratio * size.height);
    UIGraphicsBeginImageContext(rect.size);
    [pimage drawInRect:rect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Call EditMasterProductController
    EditMasterProductController *editmastprodController = [[EditMasterProductController alloc] initWithNibName:@"EditMasterProductController" bundle:nil];
    MasterProduct *selectedmastprod = (MasterProduct *)[fetchedResultsController objectAtIndexPath:indexPath];
    editmastprodController.mastprod = selectedmastprod;
    UINavigationController *navigController = [[UINavigationController alloc] initWithRootViewController:editmastprodController];
    [self presentModalViewController:navigController animated:YES];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[[self fetchedResultsController] objectAtIndexPath:indexPath ]];   // 書本範例 使用這樣
//        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath ]];       // RootViewController 使用這樣
        NSError *error;
        if (![context save:&error]) {
//            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
        }
    }
}

-(NSFetchedResultsController *)fetchedResultsController
{
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }

    probAppDelegate *appDelegate = (probAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"MasterProduct" inManagedObjectContext:context]];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"itemname" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];

    // Edit the section name key path and cache name if appropriate. 
    // nil for section name key path means “no sections”.
    NSFetchedResultsController *aFetchedResultsController = 
        [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest 
                                            managedObjectContext:context 
                                              sectionNameKeyPath:nil 
                                                       cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;

    [aFetchedResultsController release];
    [fetchRequest release];
    [sortDescriptor release];
    [sortDescriptors release];
    
    return fetchedResultsController;
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}


@end
