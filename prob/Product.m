//
//  Product.m
//  prob
//
//  Created by stronger on 11/9/22.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "Product.h"
#import "Customer.h"


@implementation Product
@dynamic itemname;
@dynamic quantity;
@dynamic price;
@dynamic customer;

@end
