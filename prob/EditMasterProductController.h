//
//  EditMasterProductController.h
//  prob
//
//  Created by stronger on 11/9/28.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterProduct.h"

@interface EditMasterProductController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    MasterProduct *mastprod;
    IBOutlet UITextField *itemname;
    IBOutlet UITextField *quantity;
    IBOutlet UITextField *price;
    IBOutlet UIImageView *prodimage;
    IBOutlet UIBarButtonItem *backbutton;
}

@property (nonatomic, retain) MasterProduct *mastprod;
@property (nonatomic, retain) IBOutlet UITextField *itemname;
@property (nonatomic, retain) IBOutlet UITextField *quantity;
@property (nonatomic, retain) IBOutlet UITextField *price;
@property (nonatomic, retain) IBOutlet UIImageView *prodimage;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *backbutton;

-(IBAction)changeimage:(id)sender;
-(IBAction)back:(id)sender;

@end
